﻿using System.Threading.Tasks;
using CyrilTypography.Backend.Controllers.Helper;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using CyrilTypography.Backend.Models;
using Microsoft.Owin.Security.DataProtection;

namespace CyrilTypography.Backend
{
    // Configure the application user manager used in this application. UserManager is defined in ASP.NET Identity and is used by the application.

    public class ApplicationUserManager : UserManager<ApplicationUser>
    {
	    internal ApplicationDbContext db = new ApplicationDbContext();

		public ApplicationUserManager(IUserStore<ApplicationUser> store)
            : base(store)
        {
			this.UserValidator = new UserValidator<ApplicationUser>(this) { AllowOnlyAlphanumericUserNames = false };
		}

        public static ApplicationUserManager Create(IdentityFactoryOptions<ApplicationUserManager> options, IOwinContext context)
        {
            var manager = new ApplicationUserManager(new UserStore<ApplicationUser>(context.Get<ApplicationDbContext>()));
            // Configure validation logic for usernames
            manager.UserValidator = new UserValidator<ApplicationUser>(manager)
            {
                AllowOnlyAlphanumericUserNames = false,
                RequireUniqueEmail = true
            };
            // Configure validation logic for passwords
            manager.PasswordValidator = new PasswordValidator
            {
                RequiredLength = 6,
                RequireNonLetterOrDigit = false,
                RequireDigit = false,
                RequireLowercase = false,
                RequireUppercase = false,
            };

			manager.EmailService = new EmailService();

			var provider = new DpapiDataProtectionProvider("AppName");

		 
			manager.UserTokenProvider = new DataProtectorTokenProvider<ApplicationUser>(
				provider.Create("PasswordReset"));

			////var provider = new DpapiDataProtectionProvider("AppName");
			////manager.UserTokenProvider = new DataProtectorTokenProvider<ApplicationUser, string>(provider.Create("PasswordReset"));
			//var dataProtectionProvider = options.DataProtectionProvider;
			//         if (dataProtectionProvider != null)
			//         {
			//             manager.UserTokenProvider = new DataProtectorTokenProvider<ApplicationUser>(dataProtectionProvider.Create("ASP.NET Identity"));
			//         }
			return manager;
        }

	}
}
