﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CyrilTypography.Backend.Controllers.AdditionModels
{
	public class ForgotPasswordViewModel
	{
		public string Email { get; set; }
	}
}