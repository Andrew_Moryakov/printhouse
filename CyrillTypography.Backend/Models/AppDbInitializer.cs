﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace CyrilTypography.Backend.Models
{
	public class AppDbInitializer : CreateDatabaseIfNotExists<ApplicationDbContext>
	{
		protected override void Seed(ApplicationDbContext context)
		{
			IdentityRole clientRole = null;
			IdentityRole adminRole = null;

			if (!context.Roles.Any(r => r.Name == AppRole.Administrator.ToString()))
			{
				RoleStore<IdentityRole> store = new RoleStore<IdentityRole>(context);
				RoleManager<IdentityRole> manager = new RoleManager<IdentityRole>(store);
				adminRole = new IdentityRole
				{
					Name = AppRole.Administrator.ToString()
					
				};

				manager.Create(adminRole);
			}

			if (!context.Roles.Any(r => r.Name == AppRole.Client.ToString()))
			{
				RoleStore<IdentityRole> store = new RoleStore<IdentityRole>(context);
				RoleManager<IdentityRole> manager = new RoleManager<IdentityRole>(store);
				clientRole = new IdentityRole
				{
					Name = AppRole.Client.ToString()

				};

				manager.Create(clientRole);
			}


			var adminUser = new ApplicationUser
			{
				Email = "admin@admin.com",
				FirstName = "admin",
				LastName = "admin",
				UserName = "admin@admin.com",
				EmailConfirmed = true
			};
			AddUser(context, adminUser, adminRole);

			//Random rndmzr = new Random();
			//for (int i = 0; i < 10; i++)
			//{
			//	var client = new ApplicationUser
			//	{
			//		Email = $"client{i}@client.com",
			//		FirstName = $"client{i}",
			//		LastName = $"client{i}",
			//		UserName = $"client{i}@client.com",
			//		CurrentBalance = rndmzr.Next(0, 1000)
			//	};
			//	AddUser(context, client, clientRole);
			//}

			context.Settings.Add(new Setting(2));

			context.SaveChanges();

			base.Seed(context);
		}


		public void AddUser(ApplicationDbContext context, ApplicationUser user, IdentityRole role)
		{
			//if (adminInDb == null)
			//{
			var userManager = new ApplicationUserManager(new UserStore<ApplicationUser>(context));

			const string password = "123456";
			var result = userManager.Create(user, password);

			if (result.Succeeded)
			{
				userManager.AddToRole(user.Id, role.Name);
			}
		}
	}
}