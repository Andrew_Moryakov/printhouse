﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CyrilTypography.Backend.Models
{
	public class InvoiceMovement
	{
		public InvoiceMovement()
		{
		}

		public InvoiceMovement(decimal mov, DateTime now, string userId, string targetUserId)
		{
			TargetUserId = targetUserId;
			Movement = mov;
			Date = now;
			UserId = userId;
		}
		[Key]
		public int Id { get; set; }
		public decimal Movement { get; set; }
		public DateTime Date { get; set; }

		public string UserId { get; set; }
		[ForeignKey("UserId")]
		public ApplicationUser User { get; set; }

		public string TargetUserId { get; set; }
	}

	public class Setting
	{
		public Setting()
		{
			
		}
		public Setting(decimal costPage)
		{
			CostPage = costPage;
		}

		[Key]
		public int Id { get; set; }
		public decimal CostPage { get; set; }
	}

	public class PrintedPage
	{
		public PrintedPage()
		{
			
		}

		public PrintedPage(string docName, string userId, int pages, DateTime date, decimal cost)
		{
			DocumentName = docName;
			UserId = userId;
			Pages = pages;
			Date = date;
			CostPage = cost;
		}

		[Key]
		public int Id { get; set; }
		public string DocumentName { get; set; }
		public int Pages { get; set; }
		public DateTime Date { get; set; }
		public decimal CostPage { get; set; }

		public string UserId { get; set; }
		[ForeignKey("UserId")]
		public ApplicationUser User { get; set; }
	}
}