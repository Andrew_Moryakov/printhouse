﻿var dropDownTemplate;
$(document).ready(function() {
	$('.shape').shape();
	$('.ui.rating').rating();
	$('.menu .item').tab();
	$(".ui.fluid.multiple.search.selection.dropdown").dropdown({ allowAdditions: true });

	$('.button.ascending').popup({
		delay: {
		show: 300,
		hide: 0
	}});
	$('.button.descending').popup({
		delay: {
			show: 300,
			hide: 0
		}
	});


    dropDownTemplate = $('<div class="ui fluid multiple search selection dropdown"> <input name="tags" type="hidden"> <i class="dropdown icon"></i> <div class="default text">Skills</div> <div class="menu"></div></div>').dropdown({ allowAdditions: true });
    $('#categoryProduct')
   .dropdown()
		;


	 $('.ui.secondary.vertical.menu')
    .on('click', '.item', function() {
      if(!$(this).hasClass('dropdown')) {
        $(this)
          .addClass('active')
          .siblings('.item')
            .removeClass('active');
      }
    });
});