﻿///Возвращает строку, которая содержит токен доступа
function getAccessTokenFromLocalStorage() {
	var tokenObject = getTokenObjFromLocalStorage();

	if (tokenObject === null) {
		return null;
	}

	return tokenObject.access_token;
}
