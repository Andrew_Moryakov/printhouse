﻿function destroy_token(){
	window.sessionStorage["CTToken"] = undefined;
	modal('success', 'Выход выполнен.');
	post("/Home/Index");
}

function reset_password(){
	$('.overlay_message').html('<i class="close icon"></i><div class="ui input"><div onsubmit="javascript:hidemessage();"><input placeholder="Введите адрес Email" type="text"><br /><br /><input type="submit" ng-click="test()" class="ui positive button" value="Восстановить пароль"></div></div>');
	showmessage();
}
function change_password(){
	$('.overlay_message').html('<i class="close icon"></i><div class="ui input"><div onsubmit="javascript:hidemessage();"><input placeholder="Введите старый пароль" type="text"><br /><br /><input placeholder="Введите новый пароль" type="text"><br /><br /><input placeholder="Подтвердите новый пароль" type="text"><br /><br /><input type="submit" class="ui positive button" value="Сменить пароль"></div></div>');
	showmessage();
}

function modal(type, html){
	$('.overlay_message').addClass(type).html(html);
	showmessage();
	setTimeout(hidemessage, 2500);
}

function singin_toggle(){
	$('[data-tab="second"]').removeClass('active');
	$('[data-tab="first"]').addClass('active');
	hidemessage();
};

function after_register(){
	$('.overlay_message').addClass('success').html('Вы успешно зарегистрировались.<br /><a href="javascript:singin_toggle();">Войти</a><i class="close icon"></i>');
	showmessage();
};

function showmessage(){
	var marginLeft = $('.overlay_message').width()/-2;
	var marginTop = $('.overlay_message').height()/-2;
	$('.overlay_message').css({marginLeft: marginLeft +'px', marginTop: marginTop +'px'});
	$('.overlay:not(.ui)').show().animate({opacity: '1'}, 200);
	$('.overlay_message').show().animate({ opacity: '1' }, 200);
}

function hidemessage(){
	$('.overlay:not(.ui)').animate({opacity: '0'}, 300);
	$('.overlay_message').animate({opacity: '0'}, 300);
	setTimeout(breakmessage, 500);
}
function breakmessage(){
	$('.overlay:not(.ui)').hide();
	$('.overlay_message').hide().removeClass('success error');
}