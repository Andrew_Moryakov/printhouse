﻿
		var beforeSend = function() {
		$(".ui.page.dimmer").dimmer("show");
	}

	var afterSend = function() {
		$(".ui.page.dimmer").dimmer("hide");
}

app.controller("restorePassCtrl",
	function($scope, $http,$sessionStorage, $window) {

		$scope.$storage = $sessionStorage;
		$scope.$watch('firstName',
			function(newValue) {
				if (newValue !== undefined)
					$log.log($scope.firstName)
			});

		$scope.authnrData = {};

		$scope.$parent.successSignUp = false;
		$scope.signIn = function () {
			var token = getToken($scope.authnrData.email, $scope.authnrData.password);
			if (token.access_token != undefined && token.access_token != null) {
				$window.sessionStorage["CTToken"] = token.access_token; // $sessionStorage.CTToken = token.access_token;
				$window.sessionStorage["CTRole"] = token.role;

				if (token.role === "Client") {
					post("/Client/Index");
				} else {
					post("/Administrator/Index");
				}
			} else {
				modal('error', 'Неверная почта или пароль.');
			}
		}
	});