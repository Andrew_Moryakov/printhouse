﻿
app.controller("ClientBalansCtrl",
	function($scope, $http, $sessionStorage, $window) {

		$scope.$storage = $sessionStorage;
		var token = $window.sessionStorage["CTToken"];//$scope.$storage.CTToken;

		if (token !== undefined) {
			$scope.data = runPost("/api/Account/ExternalUserInfo", null, token, "GET");
			$scope.docs = runPost("/api/Account/Documents", null, token, "GET");
		}

		$scope.FundAcount = function(){
			post('https://opl.k-holding.biz/');
		}
		
		$scope.resetPassword = function() {

				runPost("/api/Account/ChangePassword", $scope.resetPasswordData, token, "POST");
				$("#resetPasswordDimmer").dimmer("hide");
		}

		$scope.recoveryPassword = function () {
			runPost("/api/Account/ForgotPassword", $scope.recoveryPasswordData, token, "POST");
			$("#recoveryPasswordDimmer").dimmer("hide");
		}

		$scope.showDimmerById = function(dimerId)
		{
			$(dimerId).dimmer("show");
		}
	});