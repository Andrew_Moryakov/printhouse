﻿app.controller("ForgotPasswordCtrl",
	function($scope, $http, $sessionStorage, $window) {
		
		$scope.recoveryPassword = function () {
			runPost("/api/Account/ForgotPassword", $scope.recoveryPasswordData, null, "POST");
		}
	});