﻿app.controller("AdminCtrl",
	function($scope, $http, $sessionStorage, $window) {
		$scope.$storage = $sessionStorage;
		var token = $window.sessionStorage["CTToken"]; //$scope.$storage.CTToken;

		if (token != undefined)
			$scope.users = runPost("/api/Account/ExternalUsersInfos", null, token, "GET");


		$scope.clickByUser = function(emailOfSelectedUser) {
			$scope.selectedUser =
				Enumerable.From($scope.users).Where(function(user) { return user.Email === emailOfSelectedUser }).ToArray()[0];
		};
		$scope.DownLoadUsers = function() {
			runPost("/UsersExport/Index", null, token, "GET");
		}

		$scope.showDimerFundAccount = function () {

			if ($scope.Amount != 0 && $scope.Amount != null && $scope.Amount != undefined && $scope.selectedUser != null && $scope.selectedUser != undefined) {
				$("#dimerFundAccount").addClass('active');
			}
		}

		$scope.fundAccount = function () {
			$("#fundAccountButton").addClass( "disabled" );
			//$("#fundAccountIco").css( "visibility", "visible" );
			if ($scope.Amount == 0)
				return;

			$scope.Amount = replace($scope.Amount, ',', '.');
			$scope.selectedUser.Balance = replace($scope.selectedUser.Balance, ',', '.');

			if (sum($scope.Amount, $scope.selectedUser.Balance) < 0)
				return;

			if ($scope.Amount)
				var dataToServer = {
					"Email": $scope.selectedUser.Email,
					"Amount": $scope.Amount
				}

			var result = runPost("/api/Account/FundAccount", dataToServer, token, "POST");

			if (result.status == 200) {
				$scope.selectedUser.Balance = runPost("/api/Account/ExternalUserInfoByEmail?email=" + $scope.selectedUser.Email,
					{},
					token,
					"GET",
					"application/x-www-form-urlencoded").Balance; //.Balance = sum($scope.selectedUser.Balance, $scope.Amount);
			}
			$("#dimerFundAccount").removeClass('active');
				$("#fundAccountButton").removeClass( "disabled" );
			//$("#fundAccountIco").css( "visibility", "hidden " );
			//$("#fundAccountButton").css( "visibility", "visible " );
		}

		$scope.resetPassword = function () {

			$("#resetPasswordDimmer").removeClass('active');
		}

		$scope.recoveryPassword = function() {
			$("#recoveryPasswordDimmer").removeClass('active');
		}

		$scope.showDimmerById = function(dimerId) {
			$(dimerId).addClass('active');
		}
		
		$scope.deleteUser = function() {
			$scope.users = runPost("/api/Account/DeleteUser?Email=" + $scope.selectedUser.Email, null, token, "GET");
			$("#dimerDeleteUser").removeClass('active');
		}

		$scope.showDimerDeleteUser = function() {
			$("#dimerDeleteUser").addClass('active');
		}

		$scope.showDimerUserMoreInfo = function(user) {
			$scope.docs = runPost("/api/Account/Documents?Email=" + user.Email, null, token, "GET");
			$scope.movements = runPost("/api/Account/GetMovements?Email=" + user.Email, null, token, "GET");
			
			$("#dimerUserMoreInfo").addClass('active');
		}
	});
function replace(t, o, n) {
	return t.split(o).join(n);
}
function sum(one, two) {

     one = Number(parseFloat(one).toFixed(2)) + Number(parseFloat(two).toFixed(2));
	 return parseFloat(one).toFixed(2);
 };