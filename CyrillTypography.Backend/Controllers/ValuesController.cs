﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using CyrilTypography.Backend.Models;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;

namespace CyrilTypography.Backend.Controllers
{
    [Authorize]
    public class ValuesController : ApiController
    {
        // GET api/values
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }

		[Authorize]
		[Route("api/Values/CostPage")]
		[HttpGet]
		public string GetCostPage()
		{
			using (ApplicationDbContext db = new ApplicationDbContext())
			{
				var r = db.Settings.FirstOrDefault();

				return r.CostPage.ToString();
			}
		}
	}
}
