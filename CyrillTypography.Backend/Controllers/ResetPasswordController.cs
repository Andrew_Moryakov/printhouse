﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Mvc;
using CyrilTypography.Backend.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security.DataProtection;

namespace CyrilTypography.Backend.Controllers
{
    public class ResetPasswordController : Controller
	{
		// GET: Client
		[AllowAnonymous]
		public ActionResult Index(string code)
		{
			ViewBag.Code = code;
			return code == null ? View("Error") : View();
		}

		[HttpPost]
		[AllowAnonymous]
		[ValidateAntiForgeryToken]
		public async Task<ActionResult> ResetPassword(string code, string email, string password)
		{
			using (ApplicationDbContext db = new ApplicationDbContext())
			{
				var provider = new DpapiDataProtectionProvider("AppName");

					var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));// new UserManager<ApplicationUser>(new UserStore<ApplicationUser>());
				//var userManager = new ApplicationUserManager(new UserStore<ApplicationUser>(context.Get<ApplicationDbContext>()));
				userManager.UserTokenProvider = new DataProtectorTokenProvider<ApplicationUser>(
					provider.Create("PasswordReset"));

				var user = userManager.FindByEmail(email);
				if (user == null)
				{
					return RedirectToAction("Index", "Home");
				}
				  code = code.Replace(" ", "+");
				var result = await userManager.ResetPasswordAsync(user.Id, code, password);
				if (result.Succeeded)
				{
					return RedirectToAction("Index", "Home");
				}
				return RedirectToAction("Index", "Home");
			}
		}
	}
}
