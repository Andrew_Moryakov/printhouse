﻿using System.Collections.Generic;
using System.Configuration;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using MailKit.Net.Smtp;
using MailKit.Security;
using Microsoft.AspNet.Identity;
using MimeKit;

namespace CyrilTypography.Backend.Controllers.Helper
{
	public class EmailService : IIdentityMessageService
	{
		public Task SendAsync(IdentityMessage message)
		{
			Task t = Task.Factory.StartNew(() =>
			{
				var form = new Dictionary<string, string>
				{
					["from"] = ConfigurationManager.AppSettings.Get("restoreAccessEmail"),
					["to"] = message.Destination,
					["subject"] = message.Subject,
					["body"] = message.Body,
					["pass"] = ConfigurationManager.AppSettings.Get("restoreAccessPass"),
					["host"] = ConfigurationManager.AppSettings.Get("restoreAccessEmailHost"),
					["port"] = ConfigurationManager.AppSettings.Get("restoreAccessHostPort"),
				};

				var emailMessage = new MimeMessage();

				emailMessage.From.Add(new MailboxAddress(ConfigurationManager.AppSettings.Get("serviceDomain"), form["from"]));
				emailMessage.To.Add(new MailboxAddress(form["to"]));
				emailMessage.Subject = form["subject"];
				emailMessage.Body = new TextPart(MimeKit.Text.TextFormat.Html)
				{
					Text = form["body"]
				};

				SecureSocketOptions secureOption = bool.Parse(ConfigurationManager.AppSettings.Get("restoreAccessHostSsl")) == true
					? SecureSocketOptions.SslOnConnect
					: SecureSocketOptions.None;

				using (var client = new SmtpClient())
				{
					client.MessageSent += (sender, args) => client.Disconnect(true);

					client.Connect(form["host"], int.Parse(form["port"]), secureOption);
					client.Authenticate(form["from"], form["pass"]);
					client.Send(emailMessage);
				}
			});

			return t;
		}
	}
}