﻿using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using CyrilTypography.Backend.Controllers.Attributes;
using CyrilTypography.Backend.Models;

namespace CyrilTypography.Backend.Controllers
{
	[RoutePrefix("api/Account")]
	public class ApplicationUsersController : ApiController
	{
		private ApplicationDbContext db = new ApplicationDbContext();

		// GET: api/ApplicationUsers
		public IQueryable<ApplicationUser> GetApplicationUsers()
		{

			return (db.Users as DbSet<ApplicationUser>);
		}

		// GET: api/ApplicationUsers/5
		[ResponseType(typeof(ApplicationUser))]
		public async Task<IHttpActionResult> GetApplicationUser(string id)
		{
			ApplicationUser applicationUser = await (db.Users as DbSet<ApplicationUser>).FindAsync(id);
			if (applicationUser == null)
			{
				return NotFound();
			}

			return Ok(applicationUser);
		}

		// PUT: api/ApplicationUsers/5
		[ResponseType(typeof(void))]
		public async Task<IHttpActionResult> PutApplicationUser(string id, ApplicationUser applicationUser)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			if (id != applicationUser.Id)
			{
				return BadRequest();
			}

			db.Entry(applicationUser).State = EntityState.Modified;

			try
			{
				await db.SaveChangesAsync();
			}
			catch (DbUpdateConcurrencyException)
			{
				if (!ApplicationUserExists(id))
				{
					return NotFound();
				}
				else
				{
					throw;
				}
			}

			return StatusCode(HttpStatusCode.NoContent);
		}

		// POST: api/ApplicationUsers
		[ResponseType(typeof(ApplicationUser))]
		[HttpPost]
		[CheckModelForNull]
		[ValidateModelState]
		[AnyExceptionFilter]
		public async Task<IHttpActionResult> CreateApplicationUser(ApplicationUser applicationUser)
		{	
			//var result = ModelTools<ApplicationUser>.Make(applicationUser, EntityState.Added);

			//return Ok();

			return CreatedAtRoute("DefaultApi", new
			{
				id = applicationUser.Id
			}, applicationUser);
		}

		// DELETE: api/ApplicationUsers/5
		[ResponseType(typeof(ApplicationUser))]
		public async Task<IHttpActionResult> DeleteApplicationUser(string id)
		{
			ApplicationUser applicationUser = await (db.Users as DbSet<ApplicationUser>).FindAsync(id);
			if (applicationUser == null)
			{
				return NotFound();
			}

			(db.Users as DbSet<ApplicationUser>).Remove(applicationUser);
			await db.SaveChangesAsync();

			return Ok(applicationUser);
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				db.Dispose();
			}
			base.Dispose(disposing);
		}

		private bool ApplicationUserExists(string id)
		{
			return (db.Users as DbSet<ApplicationUser>).Count(e => e.Id == id) > 0;
		}
	}
}