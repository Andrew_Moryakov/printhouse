﻿using System.Net;
using System.Net.Http;
using System.Web.Http.Filters;

namespace CyrilTypography.Backend.Controllers.Attributes
{
	public class AnyExceptionFilterAttribute : ExceptionFilterAttribute
	{
		public override void OnException(HttpActionExecutedContext context)
		{
			context.Response = new HttpResponseMessage(HttpStatusCode.InternalServerError);
		}
	}
}