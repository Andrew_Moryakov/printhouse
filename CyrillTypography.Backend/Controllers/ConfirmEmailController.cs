﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CyrilTypography.Backend.Controllers.Helper;
using CyrilTypography.Backend.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security.DataProtection;

namespace CyrilTypography.Backend.Controllers
{
    public class ConfirmEmailController : Controller
    {
        // GET: ConfirmEmail
        public ActionResult Index(string code, string email)
        {
	        using (ApplicationDbContext db = new ApplicationDbContext())
	        {
		        var provider = new DpapiDataProtectionProvider("AppName");

		        var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));
			        // new UserManager<ApplicationUser>(new UserStore<ApplicationUser>());
		        //var userManager = new ApplicationUserManager(new UserStore<ApplicationUser>(context.Get<ApplicationDbContext>()));
		        //userManager.UserTokenProvider = new DataProtectorTokenProvider<ApplicationUser>(
		        //	provider.Create("PasswordReset"));

		        var user = userManager.FindById(code);
		        if (user.Email == email && user.Id == code)
			        UserHelper.ConfirmEmail(true, email);

		        return RedirectToAction("Index", "Home");
	        }
	        //ApplicationUser user = this.UserManager.FindById(Token);
			//if (user != null)
			//{
			//	if (user.Email == Email)
			//	{
			//		user.EmailConfirmed = true;
			//		await UserManager.UpdateAsync(user);
			//		await SignInAsync(user, isPersistent: false);
			//		return RedirectToAction("Index", "Home", new { ConfirmedEmail = user.Email });
			//	}
			//	else
			//	{
			//		return RedirectToAction("Confirm", "Account", new { Email = user.Email });
			//	}
			//}
			//else
			//{
			//	return RedirectToAction("Confirm", "Account", new { Email = "" });
			//}
		}
    }
}