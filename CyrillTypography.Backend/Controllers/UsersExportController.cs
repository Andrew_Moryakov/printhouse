﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using CyrilTypography.Backend.Controllers.Helper;
using CyrilTypography.Backend.Models;

namespace CyrilTypography.Backend.Controllers
{
	[Authorize(Roles = "Administrator")]
	public class UsersExportController : Controller
    {
        // GET: UsersExport
		[HttpGet]
        public ActionResult Index()
        {
	        string result = "";
	        using (var db = new ApplicationDbContext())
	        {	
		        result = string.Join("\r\n", db.Users.Select(el => el.Email));
	        }

			return File(new MemoryStream(Encoding.UTF8.GetBytes(result?? "")), "text/plain", "emails.txt");
        }
    }
}