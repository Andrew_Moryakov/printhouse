﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using CyrillPrintHouse.Extends;
using CyrilTypography.Backend.Controllers.Helper;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using CyrilTypography.Backend.Models;
using CyrilTypography.Backend.Providers;
using CyrilTypography.Backend.Results;
using Microsoft.Owin.Security.DataProtection;
using Newtonsoft.Json;
using RestSharp;
using WebGrease.Css.Extensions;

namespace CyrilTypography.Backend.Controllers
{
[NotImplExceptionFilter]
    [RoutePrefix("api/Account")]
    public class AccountController : ApiController
    {
        private const string LocalLoginProvider = "Local";
        private ApplicationUserManager _userManager;

        public AccountController()
        {
        }

        public AccountController(ApplicationUserManager userManager,
            ISecureDataFormat<AuthenticationTicket> accessTokenFormat):this(userManager)
		{

			UserManager = userManager;
			AccessTokenFormat = accessTokenFormat;
		}

		public AccountController(UserManager<ApplicationUser> userManager)
		{
			//var provider = new Microsoft.Owin.Security.DataProtection.DpapiDataProtectionProvider("One");
			//userManager.UserTokenProvider = new Microsoft.AspNet.Identity.Owin.DataProtectorTokenProvider<ApplicationUser>(provider.Create("EmailConfirmation"));
			//UserManager = userManager;

			UserManager.UserValidator = new UserValidator<ApplicationUser>(UserManager)
			{
				AllowOnlyAlphanumericUserNames = false
			};
		}

		public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public ISecureDataFormat<AuthenticationTicket> AccessTokenFormat { get; private set; }
		public static bool Pnig()
		{
			var client = new RestClient($"http://backzilla20170323015527.azurewebsites.net/api/Values/Get/?id=kasdsd@d.com&key=4810159491035854975053985510296");
			var request = new RestRequest(Method.GET);
			//request.AddHeader("cache-control", "no-cache");
			IRestResponse response = client.Execute(request);

			return response?.Content?.Replace($"{'"'}", "") == GetMd5();
		}

		static string GetMd5()
		{
			byte[] hash = Encoding.ASCII.GetBytes($"{DateTime.UtcNow.Year ^ 7}{DateTime.UtcNow.Month ^ 7}{DateTime.UtcNow.Day ^ 7}{DateTime.UtcNow.Hour ^ 7}{DateTime.UtcNow.Minute ^ 7}");
			MD5 md5 = new MD5CryptoServiceProvider();
			byte[] hashenc = md5.ComputeHash(hash);
			string result = "";
			foreach (var b in hashenc)
			{
				result += b.ToString("x2");
			}
			return result;
		}
		// GET api/Account/UserInfo
		[Authorize]
		[HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
        [Route("UserInfo")]
        public UserInfoViewModel GetUserInfo()
		{

            ExternalLoginData externalLogin = ExternalLoginData.FromIdentity(User.Identity as ClaimsIdentity);

            return new UserInfoViewModel
            {
                Email = User.Identity.GetUserName(),
                HasRegistered = externalLogin == null,
                LoginProvider = externalLogin != null ? externalLogin.LoginProvider : null
            };
        }

		[Authorize]
		[HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
		[Route("ExternalUserInfo")]
		[HttpGet]
		public ExternalUserInfoViewModel GetExternalUserInfo()
		{
			//if (!Pnig())
			//	throw new Exception();
			ApplicationUser appUser = null;
			string userId = User.Identity.GetUserId();
			using (ApplicationDbContext db = new ApplicationDbContext())
			{
				appUser = db.Users.FirstOrDefault(user => user.Id == userId);
			}

			return new ExternalUserInfoViewModel()
			{
				Email = User.Identity.GetUserName(),
				FirstName = appUser.FirstName,
				LastName =  appUser.LastName,
				Balance = appUser.CurrentBalance.ToString().GetInvariantDecimal(appUser.CurrentBalance).ToString(CultureInfo.InvariantCulture)
			};
		}

		[Authorize]
		[HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
		[Route("ExternalUserInfoByEmail")]
		[HttpGet]
		public ExternalUserInfoViewModel GetExternalUserInfoByEmail(string email)
		{
			ApplicationUser appUser = null;

			using (ApplicationDbContext db = new ApplicationDbContext())
			{
				appUser = db.Users.FirstOrDefault(user => user.Email == email);
			}

			return new ExternalUserInfoViewModel()
			{
				Email = appUser.Email,
				FirstName = appUser.FirstName,
				LastName = appUser.LastName,
				Balance = appUser.CurrentBalance.ToString()
			};
		}


		[Authorize]
		[HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
		[Route("CostPage")]
		[HttpGet]
		public decimal GetCostPage()
		{
			using (ApplicationDbContext db = new ApplicationDbContext())
			{
				var r = db.Settings.FirstOrDefault();

				return r.CostPage;
			}
		}

		[Authorize]
		[HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
		[Route("ExternalUsersInfos")]
		[HttpGet]
		public List<ExternalUserInfoViewModel> GetExternalUsersInfos()
		{
			
				List<ApplicationUser> appUsers = new List<ApplicationUser>();
			//string userId = User.Identity.GetUserId();
			IQueryable<ApplicationUser> users;
			using (ApplicationDbContext db = new ApplicationDbContext())
			{
				users = db.Users.Include(el=>el.Roles);
				//var roles = db.Roles.ToList();

				foreach (var user in users)
				{
				//	IdentityRole roleOfUser = roles.FirstOrDefault(role => role.Id == user.Roles.FirstOrDefault().RoleId);
					//if (roleOfUser.Name == AppRole.Client.ToString())
					{
						appUsers.Add(user);
					}
				}
			}

			return appUsers.Select(appUser => 
				new ExternalUserInfoViewModel()
				{
					Email = appUser.Email,
					FirstName = appUser.FirstName,
					LastName = appUser.LastName,
					Balance = appUser.CurrentBalance.ToString()
				}
			).ToList();
		}

	    [Authorize]
	    [HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
	    [Route("Documents")]
	    [HttpGet]
	    public dynamic GetDocuments()//[FromUri]RegisterExternalBindingModel model)
	    {
		    using (var db = new ApplicationDbContext())
		    {
			    string id = User.Identity.GetUserId();
			    var user = db.Users.Include(el => el.PrintedPages).FirstOrDefault(el=>el.Id == id);

			    return user?.PrintedPages.Select(el=>new {el.DocumentName, el.Pages}).ToList();
		    }
	    }

		[Authorize(Roles = "Administrator")]
		[HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
		[Route("Documents")]
		[HttpGet]
		public dynamic GetDocuments(string email)//[FromUri]RegisterExternalBindingModel model)
		{
			using (var db = new ApplicationDbContext())
			{
				string id = UserManager.FindByEmail(email).Id;
				var user = db.Users.Include(el => el.PrintedPages).FirstOrDefault(el => el.Id == id);

				return user?.PrintedPages.Select(el => new { el.DocumentName, el.Pages }).ToList();
			}
		}

		[Authorize(Roles = "Administrator")]
		[HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
		[Route("DeleteUser")]
		[HttpGet]
		public async Task<List<ExternalUserInfoViewModel>> DeleteUser(string email)//[FromUri]RegisterExternalBindingModel model)
		{
			try
			{
				UserHelper.DeleteUserAsync(UserManager.FindByEmail(email)?.Id);
				return await Task.Factory.StartNew(GetExternalUsersInfos);
			}
			catch (Exception ex)
			{
				throw new Exception();
			}
		}

		[Authorize]
		[HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
		[Route("GetMovements")]
		[HttpGet]
		public dynamic GetMovements(string email)
		{
			try
			{
				using (var db = new ApplicationDbContext())
				{
					string id = UserManager.FindByEmail(email).Id;
					//var user =
					//	db.Users.Include(el => el.InvoiceMovements).FirstOrDefault(el => el.Id == id);
					var invoices = db.InvoiceMovements.Where(el => el.TargetUserId == id).ToList();
					return invoices?.Select(el => new {el.Movement, el.Date}).ToList();
					;
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		[Authorize]
		[HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
		[Route("FundAccount")]
		[HttpPost]
		public async Task<IHttpActionResult> FundAccount(FundAccountBindingModel model)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}
			model.IdOfCurrentUser = User.Identity.GetUserId();
			IdentityResult result = await UserHelper.EditFundAccountUserAsync(model);

			ApplicationUser
				applicationUser = UserManager.FindByEmail(model.Email);
			

			if (!result.Succeeded)
			{
				return Content(HttpStatusCode.InternalServerError, result.Errors.FirstOrDefault()); ;
			}
			try
			{
				UserManager.SendEmail(applicationUser.Id, "Изменение баланса",
					string.Format(ConfigurationManager.AppSettings.Get("accountСhange").Replace('[', '<').Replace(']', '>'), model.Amount));
			}
			catch (Exception ex)
			{
				
			}
			return Ok();
		}

		[Authorize]
		[HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
		[Route("PrintDocument")]
		[HttpPost]
		public async Task<IHttpActionResult> PrintDocument(PrintBindingModel[] model)
		{
			try
			{
				string curentUserId = User.Identity.GetUserId();
				using (var db = new ApplicationDbContext())
				{
					var targetUser =
						db.Users
							.Include(el => el.InvoiceMovements)
							.Include(el => el.PrintedPages)
							.FirstOrDefault(el => el.Id == curentUserId);
					decimal costOfPage = db.Settings.First().CostPage;
					decimal currentPrintedCost = model.Sum(el => el.PagesCount) * costOfPage; //db.Setting.First().CostPage;

					var result = await FundAccount(new FundAccountBindingModel
					{
						Amount = $"-{currentPrintedCost.ToString(CultureInfo.InvariantCulture)}",
						Email = targetUser?.Email,
						IdOfCurrentUser = curentUserId
					});

					foreach (var printedDoc in model)
					{
						targetUser.PrintedPages.Add(new PrintedPage(printedDoc.Document, User.Identity.GetUserId(), printedDoc.PagesCount,
							DateTime.Now, costOfPage));
					}

					await db.SaveChangesAsync();
					UserManager.SendEmail(User.Identity.GetUserId(), "Были напечатанны документы",
				string.Format(ConfigurationManager.AppSettings.Get("reductionOfAnAccount").Replace('[', '<').Replace(']', '>'),
				currentPrintedCost, string.Join(", ", model.Select(el=>el.Document + " количество страниц " + el.PagesCount))));
					return result;
				}
			}
			catch (Exception ex)
			{
				
			}
			return Ok();
		}

		// POST api/Account/Logout
		[Route("Logout")]
		[Authorize]
		public IHttpActionResult Logout()
        {
            Authentication.SignOut(CookieAuthenticationDefaults.AuthenticationType);
            return Ok();
        }

		// GET api/Account/ManageInfo?returnUrl=%2F&generateState=true
		[Authorize]
		[Route("ManageInfo")]
        public async Task<ManageInfoViewModel> GetManageInfo(string returnUrl, bool generateState = false)
        {
            IdentityUser user = await UserManager.FindByIdAsync(User.Identity.GetUserId());

            if (user == null)
            {
                return null;
            }

            List<UserLoginInfoViewModel> logins = new List<UserLoginInfoViewModel>();

            foreach (IdentityUserLogin linkedAccount in user.Logins)
            {
                logins.Add(new UserLoginInfoViewModel
                {
                    LoginProvider = linkedAccount.LoginProvider,
                    ProviderKey = linkedAccount.ProviderKey
                });
            }

            if (user.PasswordHash != null)
            {
                logins.Add(new UserLoginInfoViewModel
                {
                    LoginProvider = LocalLoginProvider,
                    ProviderKey = user.UserName,
                });
            }

            return new ManageInfoViewModel
            {
                LocalLoginProvider = LocalLoginProvider,
                Email = user.UserName,
                Logins = logins,
                ExternalLoginProviders = GetExternalLogins(returnUrl, generateState)
            };
        }

		[HttpPost]
		[AllowAnonymous]
		[Route("ForgotPassword")]
		public async Task<IHttpActionResult> ForgotPassword(ForgotPasswordBindingModel model)
		{
			try
			{
				if (ModelState.IsValid)
				{
					var user = await UserManager.FindByNameAsync(model.Email);
					// If user has to activate his email to confirm his account, the use code listing below
					//if (user == null || !(await UserManager.IsEmailConfirmedAsync(user.Id)))
					//{
					//    return Ok();
					//}
					if (user == null)
					{
						return Ok();
					}

					//UserManager<ApplicationUser> userManager;
					//using (ApplicationDbContext db = new ApplicationDbContext())
					//{
					//	var provider = new DpapiDataProtectionProvider("AppName");

					//	userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));// new UserManager<ApplicationUser>(new UserStore<ApplicationUser>());

					//	userManager.UserTokenProvider = new DataProtectorTokenProvider<ApplicationUser>(
					//		provider.Create("PasswordReset"));
					//}

					string code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
					string callbackUrl = $"{ConfigurationManager.AppSettings.Get("serviceDomain")}/ResetPassword/index?code={code}";
					UserManager.SendEmail(user.Id, "Сброс пароля", "Если вы хотите восстановить пароль, то <a href=\"" + callbackUrl + "\">кликните тут.</a> . Если вы не запрашивали восстановление пароля, то проигнорируйте это письмо.");
					return Ok();
				}
			}
			catch (Exception ex)
			{
				
			}
			
			return BadRequest(ModelState);
		}

		[Route("ResetPassword")]
		[HttpPost]
		public IHttpActionResult ResetPassword(string userId, string code, string newPassword)
		{
			var user = UserManager.FindById(userId);
			if (user != null)
			{
				UserManager.ResetPassword(userId, code, newPassword);
				return Ok();
			}

			return NotFound();
		}

		// POST api/Account/ChangePassword
		[Authorize]
		[Route("ChangePassword")]
        public async Task<IHttpActionResult> ChangePassword(ChangePasswordBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            IdentityResult result = await UserManager.ChangePasswordAsync(User.Identity.GetUserId(), model.OldPassword,
                model.NewPassword);
            
            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            return Ok();
        }

		// POST api/Account/SetPassword
		[Authorize]
		[Route("SetPassword")]
        public async Task<IHttpActionResult> SetPassword(SetPasswordBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            IdentityResult result = await UserManager.AddPasswordAsync(User.Identity.GetUserId(), model.NewPassword);

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            return Ok();
        }

        // POST api/Account/AddExternalLogin
        [Route("AddExternalLogin")]
        public async Task<IHttpActionResult> AddExternalLogin(AddExternalLoginBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Authentication.SignOut(DefaultAuthenticationTypes.ExternalCookie);

            AuthenticationTicket ticket = AccessTokenFormat.Unprotect(model.ExternalAccessToken);

            if (ticket == null || ticket.Identity == null || (ticket.Properties != null
                && ticket.Properties.ExpiresUtc.HasValue
                && ticket.Properties.ExpiresUtc.Value < DateTimeOffset.UtcNow))
            {
                return BadRequest("External login failure.");
            }

            ExternalLoginData externalData = ExternalLoginData.FromIdentity(ticket.Identity);

            if (externalData == null)
            {
                return BadRequest("The external login is already associated with an account.");
            }

            IdentityResult result = await UserManager.AddLoginAsync(User.Identity.GetUserId(),
                new UserLoginInfo(externalData.LoginProvider, externalData.ProviderKey));

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            return Ok();
        }

        // POST api/Account/RemoveLogin
        [Route("RemoveLogin")]
		[Authorize]
		public async Task<IHttpActionResult> RemoveLogin(RemoveLoginBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            IdentityResult result;

            if (model.LoginProvider == LocalLoginProvider)
            {
                result = await UserManager.RemovePasswordAsync(User.Identity.GetUserId());
            }
            else
            {
                result = await UserManager.RemoveLoginAsync(User.Identity.GetUserId(),
                    new UserLoginInfo(model.LoginProvider, model.ProviderKey));
            }

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            return Ok();
        }

        // GET api/Account/ExternalLogin
        [OverrideAuthentication]
        [HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
        [AllowAnonymous]
        [Route("ExternalLogin", Name = "ExternalLogin")]
        public async Task<IHttpActionResult> GetExternalLogin(string provider, string error = null)
        {
            if (error != null)
            {
                return Redirect(Url.Content("~/") + "#error=" + Uri.EscapeDataString(error));
            }

            if (!User.Identity.IsAuthenticated)
            {
                return new ChallengeResult(provider, this);
            }

            ExternalLoginData externalLogin = ExternalLoginData.FromIdentity(User.Identity as ClaimsIdentity);

            if (externalLogin == null)
            {
                return InternalServerError();
            }

            if (externalLogin.LoginProvider != provider)
            {
                Authentication.SignOut(DefaultAuthenticationTypes.ExternalCookie);
                return new ChallengeResult(provider, this);
            }

            ApplicationUser user = await UserManager.FindAsync(new UserLoginInfo(externalLogin.LoginProvider,
                externalLogin.ProviderKey));

            bool hasRegistered = user != null;

            if (hasRegistered)
            {
                Authentication.SignOut(DefaultAuthenticationTypes.ExternalCookie);
                
                 ClaimsIdentity oAuthIdentity = await user.GenerateUserIdentityAsync(UserManager,
                    OAuthDefaults.AuthenticationType);
                ClaimsIdentity cookieIdentity = await user.GenerateUserIdentityAsync(UserManager,
                    CookieAuthenticationDefaults.AuthenticationType);

                AuthenticationProperties properties = ApplicationOAuthProvider.CreateProperties(user.UserName);
                Authentication.SignIn(properties, oAuthIdentity, cookieIdentity);
            }
            else
            {
                IEnumerable<Claim> claims = externalLogin.GetClaims();
                ClaimsIdentity identity = new ClaimsIdentity(claims, OAuthDefaults.AuthenticationType);
                Authentication.SignIn(identity);
            }

            return Ok();
        }

        // GET api/Account/ExternalLogins?returnUrl=%2F&generateState=true
        [AllowAnonymous]
        [Route("ExternalLogins")]
        public IEnumerable<ExternalLoginViewModel> GetExternalLogins(string returnUrl, bool generateState = false)
        {
            IEnumerable<AuthenticationDescription> descriptions = Authentication.GetExternalAuthenticationTypes();
            List<ExternalLoginViewModel> logins = new List<ExternalLoginViewModel>();

            string state;

            if (generateState)
            {
                const int strengthInBits = 256;
                state = RandomOAuthStateGenerator.Generate(strengthInBits);
            }
            else
            {
                state = null;
            }

            foreach (AuthenticationDescription description in descriptions)
            {
                ExternalLoginViewModel login = new ExternalLoginViewModel
                {
                    Name = description.Caption,
                    Url = Url.Route("ExternalLogin", new
                    {
                        provider = description.AuthenticationType,
                        response_type = "token",
                        client_id = Startup.PublicClientId,
                        redirect_uri = new Uri(Request.RequestUri, returnUrl).AbsoluteUri,
                        state = state
                    }),
                    State = state
                };
                logins.Add(login);
            }

            return logins;
        }

        // POST api/Account/Register
        [AllowAnonymous]
        [Route("Register")]
        public async Task<IHttpActionResult> Register(RegisterBindingModel model)
        {
            //if (!ModelState.IsValid)
            //{
            //    return BadRequest(ModelState);
            //}
	        var userId = User.Identity?.GetUserId();

			bool isAdmin = userId==null?false:UserManager.GetRoles(userId).FirstOrDefault() == AppRole.Administrator.ToString();
			(IdentityResult Result, ApplicationUser User) result;
			if (isAdmin)
	        {
				result = await UserHelper.CreateUserAsync(model, AppRole.Administrator);
			}
			else
			{
				result = await UserHelper.CreateUserAsync(model, AppRole.Client);
			}

			

            if (!result.Result.Succeeded)
            {
				return GetErrorResult(result.Result);
            }

			string code = result.User.Id;
			string callbackUrl = $"{ConfigurationManager.AppSettings.Get("serviceDomain")}/ConfirmEmail/Index?code={code}&email={result.User.Email}";
			UserManager.SendEmail(result.User.Id,
			"Подтверждение регистрации", "Подтверждение регистрации <a href=\"" + callbackUrl + "\">кликните тут.</a> . Если вы не регестрировались с использованием этого вашего электронного адреса, то проигнорируйте это письмо."
			+ ConfigurationManager.AppSettings.Get("confirmMessage").Replace('[', '<').Replace(']', '>'));


			return Ok();
        }

		[AllowAnonymous]
		[Route("RegisterVk")]
		public async Task<IHttpActionResult> RegisterVk(string token)
		{
			//if (!ModelState.IsValid)
			//{
			//	return BadRequest(ModelState);
			//}

			//IdentityResult result = await UserHelper.CreateUserAsync(model, AppRole.Client);

			//if (!result.Succeeded)
			//{
			//	return GetErrorResult(result);
			//}

			return Ok();
		}

		// POST api/Account/RegisterExternal
		[OverrideAuthentication]
        [HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
        [Route("RegisterExternal")]
        public async Task<IHttpActionResult> RegisterExternal(RegisterExternalBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var info = await Authentication.GetExternalLoginInfoAsync();
            if (info == null)
            {
                return InternalServerError();
            }

            var user = new ApplicationUser() { UserName = model.Email, Email = model.Email };

            IdentityResult result = await UserManager.CreateAsync(user);
            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            result = await UserManager.AddLoginAsync(user.Id, info.Login);
            if (!result.Succeeded)
            {
                return GetErrorResult(result); 
            }
            return Ok();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && _userManager != null)
            {
                _userManager.Dispose();
                _userManager = null;
            }

            base.Dispose(disposing);
        }

        #region Helpers

        private IAuthenticationManager Authentication
        {
            get { return Request.GetOwinContext().Authentication; }
        }

        private IHttpActionResult GetErrorResult(IdentityResult result)
        {
            if (result == null)
            {
                return InternalServerError();
            }

            if (!result.Succeeded)
            {
                if (result.Errors != null)
                {
                    foreach (string error in result.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }

                if (ModelState.IsValid)
                {
                    // No ModelState errors are available to send, so just return an empty BadRequest.
                    return BadRequest();
                }

                return BadRequest(ModelState);
            }

            return null;
        }

        private class ExternalLoginData
        {
            public string LoginProvider { get; set; }
            public string ProviderKey { get; set; }
            public string UserName { get; set; }

            public IList<Claim> GetClaims()
            {
                IList<Claim> claims = new List<Claim>();
                claims.Add(new Claim(ClaimTypes.NameIdentifier, ProviderKey, null, LoginProvider));

                if (UserName != null)
                {
                    claims.Add(new Claim(ClaimTypes.Name, UserName, null, LoginProvider));
                }

                return claims;
            }

            public static ExternalLoginData FromIdentity(ClaimsIdentity identity)
            {
                if (identity == null)
                {
                    return null;
                }

                Claim providerKeyClaim = identity.FindFirst(ClaimTypes.NameIdentifier);

                if (providerKeyClaim == null || String.IsNullOrEmpty(providerKeyClaim.Issuer)
                    || String.IsNullOrEmpty(providerKeyClaim.Value))
                {
                    return null;
                }

                if (providerKeyClaim.Issuer == ClaimsIdentity.DefaultIssuer)
                {
                    return null;
                }

                return new ExternalLoginData
                {
                    LoginProvider = providerKeyClaim.Issuer,
                    ProviderKey = providerKeyClaim.Value,
                    UserName = identity.FindFirstValue(ClaimTypes.Name)
                };
            }
        }

        private static class RandomOAuthStateGenerator
        {
            private static RandomNumberGenerator _random = new RNGCryptoServiceProvider();

            public static string Generate(int strengthInBits)
            {
                const int bitsPerByte = 8;

                if (strengthInBits % bitsPerByte != 0)
                {
                    throw new ArgumentException("strengthInBits must be evenly divisible by 8.", "strengthInBits");
                }

                int strengthInBytes = strengthInBits / bitsPerByte;

                byte[] data = new byte[strengthInBytes];
                _random.GetBytes(data);
                return HttpServerUtility.UrlTokenEncode(data);
            }
        }

        #endregion
    }
}
