﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http.Filters;
using CyrillPrintHouse.Extends;
using NLog;

namespace CyrilTypography.Backend
{
	public class NotImplExceptionFilterAttribute : ExceptionFilterAttribute
	{
		public override void OnException(HttpActionExecutedContext context)
		{
			LogManager.GetCurrentClassLogger().WriteException(context.Exception);

			throw context.Exception;
		}
	}
}