﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using NLog;

namespace CyrillPrintHouse.Extends
{
	public static class NLogExceptionExtension
	{
		public static void WriteException(this Logger logger, Exception ex, string message = null)
		{
			logger.Error(message);
			Type t = typeof(Exception);
			// Get the public properties.
			PropertyInfo[] propInfos = t.GetProperties(BindingFlags.Public | BindingFlags.Instance);

			logger.Error("---------------------- exception ----------------------");
			foreach (var property in propInfos)
			{
				if (property.Name == "InnerException")
				{
					Exception innerException = property.GetValue(ex, null) as Exception;

					if (innerException != null)
					{
						logger.Error( "------------------- inner exception -------------------");
						WriteException(logger, innerException, "inner exception");
						logger.Error("---------------- end of inner exception ---------------");
					}
				}
				else
					logger.Error($"{property.Name}:{property.GetValue(ex, null)}");
			}
			logger.Error("---------------------- end exception ----------------------");

		}
	}
}
