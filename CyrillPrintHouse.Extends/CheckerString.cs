﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CyrillPrintHouse.Extends
{
	public static class CheckerString
	{
		public static bool IsNullOrEmptyOrSpace(this string str)
		{
			return string.IsNullOrWhiteSpace(str) && string.IsNullOrEmpty(str);
		}
	}
}
