﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CyrillPrintHouse.Extends
{
	public static class StringDecimalSafeParceExtend
	{
		public static decimal GetInvariantDecimal(this string value, decimal defaultValue)
		{
			CultureInfo culture = CultureInfo.InvariantCulture;
			if (value.Contains(","))
			{
				culture = new CultureInfo("ru-ru");
			}
			else if (value.Contains("."))
			{
				culture = new CultureInfo("en-US");
			}

			bool isParsed = decimal.TryParse(value, NumberStyles.AllowDecimalPoint | NumberStyles.AllowParentheses | NumberStyles.AllowThousands, culture, out decimal result);
			if (isParsed)
			{
				return result;
			}

			return defaultValue;
		}
	}
}
