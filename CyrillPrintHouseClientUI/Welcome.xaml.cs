﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CyrillPrintHouse.Extends;
using DreamPlace.Lib.Rx;

namespace CyrillPrintHouseClientUI
{
	/// <summary>
	/// Interaction logic for Welcome.xaml
	/// </summary>
	public partial class Welcome : Page
	{
		private UserAuthInfo _userAuthInfo = null;
		private UserInfo _userInfo = null;
		private ClientOfServers _clientOfServers;

		public Welcome()
		{
			InitializeComponent();

			_userAuthInfo = Registry<UserAuthInfo, UserAuthInfo>.GetValue();
			_userInfo = Registry<UserInfo, UserInfo>.GetValue();

			textBlockBalance.Text = _userInfo?.Balance.ToString(CultureInfo.InvariantCulture).GetInvariantDecimal(_userInfo.Balance).ToString(CultureInfo.InvariantCulture);
			textBlockName.Text = $"{_userInfo?.FirstName} {_userInfo?.LastName}";

			_clientOfServers = new ClientOfServers(
				ConfigurationManager.AppSettings.Get("serviceDomain"),
				ConfigurationManager.AppSettings.Get("printControllerHost")
				);
		}

		private void ButtonLogOut_OnClick(object sender, RoutedEventArgs e)
		{
			_clientOfServers.LogOut();
			Environment.Exit(0);
		}

		private void ButtonBalanseRefresh_OnClick(object sender, RoutedEventArgs e)
		{
			string passw = Registry<UserInfo, string>.GetValue("password");
			   textBlockBalance.Text = _clientOfServers.GetUserNameInfo(_userInfo.Email, passw).Balance.ToString();
			_clientOfServers.RefrasheBalanse();
		}
	}
}
