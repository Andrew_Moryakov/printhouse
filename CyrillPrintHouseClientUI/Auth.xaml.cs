﻿using System;
using System.Collections.Generic;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CyrillPrintHouse.Extends;
using DreamPlace.Lib.Rx;
using Newtonsoft.Json;
using RestSharp;
using System.Configuration;
using System.Diagnostics;
using System.Globalization;
using System.Runtime.InteropServices;
using System.Security;
using DietManagerStudio.RegistryMecanisme;
using NLog;

namespace CyrillPrintHouseClientUI
{
	/// <summary>
	/// Interaction logic for Auth.xaml
	/// </summary>
	public partial class Auth : Page
	{
		private string userName = "";
		private string password ;
		private UserAuthInfo _token = null;
		private bool _isAuth = false;
		private readonly ClientOfServers _clientOfServers;
		private Logger _logger;
		
		public Auth()
		{
			InitializeComponent();
			_clientOfServers = new ClientOfServers(
				ConfigurationManager.AppSettings.Get("serviceDomain"),
				ConfigurationManager.AppSettings.Get("printControllerHost")
				);
			TextBlockHelp.Text += ConfigurationManager.AppSettings.Get("serviceDomain");
			_logger = LogManager.GetCurrentClassLogger();
			//TextBoxUrl.Text = ConfigurationManager.AppSettings.Get("serviceDomain");
		}

		private void buttonAuth_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				string password = textBoxPassword.Password;
				string userName = textBoxUserName.Text;
				if (password.IsNullOrEmptyOrSpace() || userName.IsNullOrEmptyOrSpace())
				{
					textBlockInfo.Text = "Введите электронную почту и пароль";
					return;
				}

				UserInfo userInfo = _clientOfServers.GetUserNameInfo(userName, password);
				_logger.Info(userInfo.Balance);
				
				Registry<UserInfo, UserInfo>.PublicValue(userInfo);
				Registry<UserInfo, string>.PublicValue(password, "password");
				if (userInfo == null)
				{
					_logger.WriteException(new ArgumentException("Не удалось полчить пользователя"));
				}

				if (userInfo.Email != null)
				{
					if (userInfo.Balance > 0)
					{
						_logger.Info("Try login in print controller");
						_clientOfServers.LogIn(userName, password);
						_logger.Info("Logined in print controller");
						_isAuth = true;
						if(Registry<Frame, MainWindow>.GetValue()!=null)
							Registry<Frame, MainWindow>.GetValue().Content = new Welcome();
					}
					else
					{
						textBlockInfo.Text = $"Вы не можете войти, ваш баланс равен {userInfo.Balance}";
					}
				}
				else
				{
					textBlockInfo.Text = "Введен неверный пароль или электронная почта";
					_logger.Info($"False auth dates  {userInfo.Email}");
					_isAuth = false;
				}
			}
			catch (Exception ex)
			{
				textBlockInfo.Text = $"Ой, что-то пошло не так, мы не можем дать Вам доступ к системе, возможно отсутствует подключение к интернету. Обратитесь к Администратору";
				_logger.WriteException(ex);
			}
		}
		private class NativeMethods
		{
			[DllImport("user32.dll")]
			public static extern bool SetForegroundWindow(IntPtr hWnd);

			[DllImport("user32.dll", SetLastError = true)]
			public static extern IntPtr FindWindow(string lpClassName, string lpWindowName);

			[DllImport("user32.dll", SetLastError = true)]
			public static extern IntPtr GetWindow(IntPtr hWnd, GetWindow_Cmd uCmd);

			public enum GetWindow_Cmd : uint
			{
				GW_HWNDFIRST = 0,
				GW_HWNDLAST = 1,
				GW_HWNDNEXT = 2,
				GW_HWNDPREV = 3,
				GW_OWNER = 4,
				GW_CHILD = 5,
				GW_ENABLEDPOPUP = 6
			}
		}
		private bool isBrowserActiveWas = false;
		private void ButtonSignUp_OnClick(object sender, RoutedEventArgs e)
		{
			Process.Start(AppDomain.CurrentDomain.BaseDirectory + "\\bat.bat");
			Registry<Frame, MainWindow>.GetValue().WindowState = WindowState.Minimized;
			//

			//Process process = new Process();
			//process.StartInfo.FileName = @"C:\Program Files\Google\Chrome\Application\chrome.exe";
			//process.StartInfo.Arguments = ConfigurationManager.AppSettings.Get("serviceDomain") + " --new-window";
			//process.Start();
			//Process.Start(@"chrome.exe",
			//	  ConfigurationManager.AppSettings.Get("serviceDomain") + " --new-window");
			//Process.Start("chrome.exe");
			//ChromiumWebBrowser chrom = new ChromiumWebBrowser();
			//chrom.Address = ConfigurationManager.AppSettings.Get("serviceDomain");
			//chrom.BrowserSettings= new BrowserSettings();
			//webBrowser.ChromiumWebBrowser.Rendering += (o, args) =>
			//{
			//	Registry<Frame, Frame>.GetValue().Content = webBrowser;

			//};

			//Registry<Frame, MainWindow>.GetValue().Content = Registry<WebBrowser, WebBrowser>.GetValue(); // webBrowser;

			//var chrom = Registry<ChromiumWebBrowser, ChromiumWebBrowser>.GetValue("global");

			//Registry<WebBrowser, ChromiumWebBrowser>.OnNext(this,
			//new RegistryEventArgs<ChromiumWebBrowser>(chrom, ActionMode.Update), null);
			//isBrowserActiveWas = true;
			//webBrowser.SizeChanged += (o, args) =>
			//{

			//	webBrowser.ChromiumWebBrowser.Height = webBrowser.ActualHeight;
			//webBrowser.ChromiumWebBrowser.Width = webBrowser.ActualWidth;
			//};
		}

		private void TextBoxPassword_OnKeyDown(object sender, KeyEventArgs e)
		{
			if (e.Key == Key.Enter)
			{
				e.Handled = true;
				buttonAuth_Click(null, null);
			}
			
		}
	}
}