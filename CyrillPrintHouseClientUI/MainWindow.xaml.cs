﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DietManagerStudio.RegistryMecanisme;
using DreamPlace.Lib.Rx;

namespace CyrillPrintHouseClientUI
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		private ClientOfServers _clientOfServers;

		public MainWindow()
		{
			InitializeComponent();
			//Registry < WebBrowser, WebBrowser>.PublicValue(new WebBrowser());
			this.Content = new Auth();
			Registry<Frame, MainWindow>.PublicValue(this);

			_clientOfServers = new ClientOfServers(
				ConfigurationManager.AppSettings.Get("serviceDomain"),
				ConfigurationManager.AppSettings.Get("printControllerHost")
				);

			//CefSharp.CefSettings settings = new CefSharp.CefSettings();
			////settings.PackLoadingDisabled = true;
			////settings.CefCommandLineArgs.Add("disable-gpu-vsync", "1");
			////settings.CefCommandLineArgs.Add("disable-gpu", "1");
			//var chr = Registry<ChromiumWebBrowser, ChromiumWebBrowser>.GetValue("global");
			//if (chr != null)
			//{
			//	chr.Reload();//Address = ConfigurationManager.AppSettings.Get("serviceDomain");
			//}

			//if (!Cef.IsInitialized && CefSharp.Cef.Initialize(settings))
			//{
			//	ChromiumWebBrowser chrom = new ChromiumWebBrowser()
			//	{
			//		//BrowserSettings = new BrowserSettings
			//		//{
			//		//	OffScreenTransparentBackground = false
			//		//},
			//		Height = this.ActualHeight
			//	};
			//	this.SizeChanged += (o, args) =>
			//	{

			//		chrom.Height = this.ActualHeight;
			//		chrom.Width = this.ActualWidth;
			//	};
			//	//ContentControl1.Children.Add(chrom); // main_grid.Children.Add(chrom);
			//	chrom.Address = ConfigurationManager.AppSettings.Get("serviceDomain"); // chrom.Address = "http://www.google.co.uk";
			//	chrom.Load(ConfigurationManager.AppSettings.Get("serviceDomain"));
			//	Registry<ChromiumWebBrowser, ChromiumWebBrowser>.PublicValue(chrom, "global");
			//Grid.SetRow(chrom, 1);
			//}

			//	webBrowser.ChromiumWebBrowser.Address = ConfigurationManager.AppSettings.Get("serviceDomain");

		}

		private void MyFrame_OnContentRendered(object sender, EventArgs e)
		{
			//this.NavigationUIVisibility = System.Windows.Navigation.NavigationUIVisibility.Hidden;
		}

		private void MainWindow_OnClosing(object sender, CancelEventArgs e)
		{
				_clientOfServers.LogOut();
		}

		private void MyFrame_OnKeyDown(object sender, KeyEventArgs e)
		{
			//if (e.Key == Key.Back)
			//{
			//	e.Handled = true;
			//}
		}

		private void MainWindow_OnKeyDown(object sender, KeyEventArgs e)
		{
			//if (e.Key == Key.Back)
			//{
			//	e.Handled = true;
			//}
		}

		private void MyFrame_OnNavigating(object sender, NavigatingCancelEventArgs e)
		{
			//if (e.NavigationMode == NavigationMode.Back)
			//{
			//	e.Cancel = true;
			//}
		}
	}
}
